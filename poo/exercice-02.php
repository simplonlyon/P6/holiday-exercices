<?php

/**
 * Exercice 02 : Créer une classe BackPack qui permettra de transporter des objets, elle aura comme propriétés : capacity (float) initialisé à 0,
 * items (array d'Item) initialisé à []
 * Elle aura comme méthode :
 * - Une méthode totalWeight() qui fera une boucle sur les items de la classe pour additionner leur weight et obtenir le poid actuel du sac
 * - Une méthode addItem() qui attendra comme argument un Item et qui viendra s'ajouter au tableau items de la classe
 *   seulement si le poid de l'item ne fait pas dépasser la capacity du sac
 * - Une méthode useItem() qui attendra un index en argument et qui fera un use sur l'item à l'index indiqué, si celui ci existe
 * 
 * Modification de la méthode useItem liée à l'exercice 03 : on entourre le use de l'item d'un try-catch, et si on tombe dans le catch, on
 * supprime l'item en question de la liste d'item
 */

 
 //Lancer ce fichier une fois le code terminé

 require 'Item.php';
 require 'BackPack.php';

 $item = new Item("Flashlight", 1.3, "light the way");
 $item2 = new Item("Bottle", 0.8, "contains liquid");
 $item3 = new Item("Book", 1.4, "pass the time");
 
 $pack = new BackPack(3.0);

 $pack->addItem($item);
 $pack->addItem($item2);
 $pack->addItem($item3); //message sac trop plein

 $pack->useItem(0);
 $pack->useItem(1);