<?php

/**
 * La récursivité pourra être utilisée pour parcourir des "choses" pouvant avoir plusieurs dimension, par exemple, un tableau contenant 
 * d'autres tableaux.
 * 
 * Exercice 02 : Faire une fonction qui fait une boucle sur un tableau (un foreach) qui :
 * - affichera la valeur si c'est une valeur simple (genre un string ou un number)
 * - se relancera elle même si la valeur actuelle est un tableau afin de parcourir le tableau de manière récursive
 */