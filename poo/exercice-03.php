<?php

/**
 * Exercice 03 : Créer une nouvelle classe Consumable qui héritera de la classe Item mais qui aura également une propriété quantity (int)
 * Surcharger la méthode use() pour y indiquer que si la quantity est supérieure à 0, on lance la méthode use de la classe parent et on réduit la 
 * quantity de 1, sinon, on declenche une exception. Surcharger aussi la méthode getWeight() pour faire que le poid de l'item soit multiplié par la quantité
 * Ensuite modifier la méthode use du BackPack pour faire en sorte que si l'utilisation de l'item déclenche une erreur, alors on retire cet item
 * du sac (avec un try-catch).
 */

 
 
 //Lancer ce fichier une fois le code terminé

 require 'Item.php';
 require 'BackPack.php';
 require 'Consumable.php';

 $item = new Item("Flashlight", 1.3, "light the way");
 $item2 = new Item("Bottle", 0.8, "contains liquid");
 $item3 = new Consumable("Banana", 0.4, "tastes like yellow", 2);
 
 $pack = new BackPack(4.0);

 $pack->addItem($item);
 $pack->addItem($item2);
 $pack->addItem($item3);

 $pack->useItem(0);
 $pack->useItem(2);
 $pack->useItem(2);
 $pack->useItem(2);

 var_dump($pack); //Le sac ne doit plus contenir que 2 items