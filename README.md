# Cahier de Vacances Simplon

Voici quelques exercices pour les personnes intéressées. Les corrections se trouvent sur la branch `solution`. Il y a également une branch clues sur laquelle vous avez des indices pour certains exercices.

C'est principalement des exercices d'algorithmie, vous n'avez donc pas besoin de servir (à part si précisé autrement) et pouvez simplement
lancer l'exercice avec php pour voir si ça fonctionne. Exemple : `php recursivite/exercice-01.php`

Ces exercices sont optionnels, ne passez pas trop trop de temps dessus histoire de profiter quand même des vacances si vous le pouvez.

