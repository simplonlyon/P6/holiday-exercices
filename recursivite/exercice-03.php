<?php


/**
 * Exercice 3 : Créer une fonction récursive qui prendra en argument le chemin d'un dossier et qui affichera tous les fichiers contenus dans
 * ce dossier et ses sous dossier. Attention à bien prendre en compte les dossier "." et ".." pour ne pas les parcourir sous peine de boucle infinies.
 */