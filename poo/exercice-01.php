<?php

/**
 * Le but de cette série d'exercice va être de créer sous forme d'objets PHP un système de sac à dos pouvant contenir des items utilisable.
 * Il permettra de pratiquer la création de classe, la composition, l'héritage.
 * 
 * Exercice 01 : Créer un fichier contenant une classe Item qui aura comme propriétés : name (string), weight (float), effect (string)
 * Elle aura également une méthode use qui fera un echo de "I use the $name, it $effect" pour afficher l'objet utiliser et son effet en gros
 */

 //Lancer ce fichier une fois le code terminé

 require 'Item.php';

 $item = new Item("Flashlight", 1.3, "light the way");

 $item->use();