<?php

/**
 * La récursivité désigne le fait d'avoir par exemple une fonction qui s'appelle elle même sous certaines conditions.
 * C'est une notion assez puissante de la programmation qui permet de faire certaines choses similaires à des boucles.
 * 
 * exemple de fonction récursive qui affiche 10 fois une phrase
 */

 //La fonction prend le compte actuel en argument (par défaut, 0)
 function recursiveLoop($count = 0) {
     //affichage de la phrase
    echo "bloup";
    //incrémentation du compte
    $count++;
    //Si le compte est inférieur à 10...
    if($count < 10) {
        //...on relance la même fonction en lui donnant le compte incrémenté de 1
        recursiveLoop($count);
    }
 }



 /**
  * Exercice 1 : Faire une fonction qui attend un tableau en argument et qui parcours ce tableau via de la récursivité.
  * 
  */